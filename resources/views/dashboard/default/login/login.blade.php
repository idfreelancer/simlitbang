@include('login.header_login')

<body class="login">
    <div class="row" style="padding : 20px 15% 0 15%">
        <div class="col-md-3">
            {!!  Theme::img('images/logo_lipi.png') !!}
        </div>
        <div class="col-md-9">
            <h1 style="font-size: 32px;text-align:left;font-weight : bold; font-family: 'Maven Pro', sans-serif;">
                {!!   Config::get('qilara.site_name') !!}
            </h1>
        </div>
    </div>

    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
            @if (count($errors) > 0)
                <div class="alert alert-danger" style="margin-bottom : 50px">
                    <ul style="list-style : none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

          <section class="login_content">
            <form action="{{ route('dashboard.login_process')}}" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            <h1>Login Form</h1>
            <div>
                <input type="text" class="form-control" placeholder="Username" required="" name="username" value="{{ old('username') }}" />
            </div>
            <div>
                <input type="password" class="form-control" placeholder="Password" required="" name="password" />
            </div>
              <div>
                <button type="submit" class="btn btn-success">
                    Sign In
                </button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">               

                <div class="clearfix"></div>
                <br />

                <div>                  
                  <p>©2016 lims.id</p>
                </div>
              </div>
            </form>
          </section>
        </div>        
      </div>
    </div>
    {!! Theme::js('js/jquery.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function(){
            $('input[name=username]').focus();
        });
    </script>
  </body>
</html>