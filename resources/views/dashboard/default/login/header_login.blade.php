<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login - {{ Config::get('qilara.site_name') }}</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:900' rel='stylesheet' type='text/css'>
    {!! Theme::css("css/bootstrap.min.css") !!}
    {!! Theme::css("css/font-awesome.min.css") !!}
    {!! Theme::css("css/animate.min.css") !!}    
    {!! Theme::css("css/custom.css") !!}
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro:900" rel="stylesheet">
</head>
