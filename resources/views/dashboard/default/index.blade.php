<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> {!! empty($title)? "" : $title !!} | {{ strip_tags(Config::get('qilara.site_name')) }}</title>
    @include('partial.header')    
</head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> 
              <span>{{ Config::get('qilara.institution_short') }}</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
                <div class="profile_pic">
                    @if (empty(Auth::user()->foto) || File::exists('uploads/'.Auth::user()->foto) == false)
                        <img src="{{Theme::url('images/user.png')}}" class="img-circle profile_img" id="image_upload">
                    @else
                        <img src="{{Theme::url('uploads/'.Auth::user()->foto)}}" class="img-circle profile_img" id="image_upload">
                    @endif                    
                    <form action="{{ route('users.store_picture') }}" method="POST" class="upload_profile_pic" enctype="multipart/form-data">
                        <input type="file" id="avatar_upload" name="avatar" style="display: none;" />
                    </form>
                </div>
                <div class="profile_info">
                    <span>Welcome,</span>
                    <h2>{{ Auth::user()->name }}</h2>
                </div>
            </div>
            <!-- /menu profile quick info -->

            <br />
            @include('partial.menu')
          </div>
        </div>

       @include('partial.top_nav')

        <!-- page content -->
        <div class="right_col" role="main">
            @if (Session::has('message'))
                <div class="row margin-top-60">
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            @endif

            @if ($errors->any())
                <div class="row margin-top-60">
                    <div class="alert alert-error" role="alert">
                        <p>{{$errors->first()}}</p>
                    </div>
                </div>
            @endif
        
            @yield('content')            
        </div>
        <!-- /page content -->
@include('partial.footer')
@yield('footer_asset')
{!! Theme::js('js/custom.js') !!}
{!! Theme::js('js/qilara.js')!!}
</body>

</html>