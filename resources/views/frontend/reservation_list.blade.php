@extends('index')

@section('content')
	@if ($errors->any())
        <div class="row">
            <div class="alert alert-error" role="alert">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif
    
	<div class="section type-2">
		<div class="container">
	    	<div class="section-headlines">
	        	<h4>Reservasi</h4>
	            <div class="row">
	            	<table class="table table-striped responsive-utilities jambo_table" id="table_wdelete" style="width: 100%">
                        <thead>
                        <tr>
                            <th>Nama Instrument</th>
                            <th>Tanggal Reservasi</th>
                            <th>Status</th>                                
                            <th>Catatan</th>
                        </tr>
                        </thead>
                        <tbody>
                        	@if (empty($data))
                            <tr>
                                <td colspan="4"><strong>Tidak Ada Data</strong></td>
                            </tr>
                            @endif

                            @foreach($data as $row)
                            <tr>
                                <td>
                                    <a href="{{ route('reservation', $row['instrument']['id']) }}" class="label label-info">
                                        {{ $row['instrument']['name'] }}
                                    </a>
                                </td>
                                <td>{{ localeDate($row['reservation_date'], false)}}</td>
                                <td>
                                	<span class="label label-success">
                                        {{ trans('common.reservation_status')[$row['status']] }}
                                    </span>                                	
                                </td>
                                <td>{{ $row['notes']}}</td>
                            </tr>
                            @endforeach
                        </tbody>
					</table>	            	
	            </div>
			</div>            
	    </div>
	</div>
@stop
