@extends('index')

@section('content')
	@if ($errors->any())
        <div class="row">
            <div class="alert alert-error" role="alert">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif
    
	<div class="section type-2">
		<div class="container">
	    	<div class="section-headlines">
	        	<h4>Reservasi</h4>
	            <div class="row">
	            	<div class="col-lg-5 col-md-5">
	            		<div id="myCarousel" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
						    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						    <li data-target="#myCarousel" data-slide-to="1"></li>
						    <li data-target="#myCarousel" data-slide-to="2"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner">
						  	@foreach ($images as $val)
								<div class="item active">
						      		<img src="{!! asset('storage/'. $val->filename) !!}" alt="New York">
						    	</div>
						  	@endforeach
						  	
						  </div>

						  <!-- Left and right controls -->
						  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#myCarousel" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
	            	</div>
	            	<div class="col-md-offset-1 col-lg-6 col-md-6">
            			@if (\Auth::guard('customer')->check())
							    <div class="row">
							        <div class='col-sm-12'>
							        	{!! Form::open(array('url' => route('reservation.store', Request::segment(2)), 'class' => 'form-horizontal', 'method' => 'post', )) !!}            		
								    	<div class="form-body">
								            <div class="form-group">
								                <label for="inputName" class="col-md-2 control-label col-xs-2">
								                    Tanggal
								                </label>
								                <div class="col-md-8 col-xs-8">
								                	<div class='input-group date' id='datetime'>
									                    <input type='text' class="form-control" name="reservation_date"/>
									                    <span class="input-group-addon">
									                        <span class="glyphicon glyphicon-calendar"></span>
									                    </span>
									                </div> 
								                </div>
								            </div>
								            <div class="form-actions pal">
								                <div class="form-group mbn">
								                    <div class="col-md-offset-2 col-md-3" >
								                        <button type="submit" class="btn btn-primary">Submit</button>
								                    </div>
								                </div>
								            </div>           
							        </div>
							    </div>
							</div>
            			@else
            				<div class="alert alert-danger" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
							  	<span class="sr-only">Error:</span>
							  		Anda harus login untuk melakukan reservasi. 
            						Klik <a href='{{route('register')}}'>disini</a> bila belum melakukan registrasi.
							</div>
            			@endif
	                </div>
	            </div>

	             <div class="row">
	            	<div class="col-lg-12 col-md-12">
	            		<table class="table table-striped responsive-utilities jambo_table col-lg-12 col-md-12">
                            <tbody> 
                            	<tr>
                            		<td class="col-lg-2 col-md-2">{{ trans('common.name') }}</td>
                            		<td>{{ $instrument['name'] }}</td>
                            	</tr>
                            	<tr>
                            		<td>{{ trans('instrument::modules.brand') }}</td>
                            		<td>{{ $instrument['brand'] }}</td>
                            	</tr>
                            	<tr>
                            		<td>{{ trans('common.year') }}</td>
                            		<td>{{ $instrument['year'] }}</td>
                            	</tr>
                            	<tr>
                            		<td>{{ trans('instrument::modules.purpose') }}</td>
                            		<td>{!! $instrument['purpose'] !!}</td>
                            	</tr>
                            	<tr>
                            		<td>{{ trans('instrument::modules.specs') }}</td>
                            		<td>{!! $instrument['specs'] !!}</td>
                            	</tr>
                            	<tr>
                            		<td>{{ trans('instrument::modules.instrument_procedure') }}</td>
                            		<td>
                            			@foreach ($user_guide as $val)
                            				<a href="{!! asset('storage/'. $val->filename) !!}" parent="_blank">{!! $val->original_name !!}</a>
                            			@endforeach
                            		</td>
                            	</tr>
                            </tbody>
                        </table>	
	            	</div>
	            </div>
			</div>            
	    </div>
	</div>
@stop

@section('footer_asset')    


   <script type="text/javascript" src="{!! asset('vendor/moment.min.js')!!}"></script>
   <script type="text/javascript" src="{!! asset('vendor/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js')!!}">
   </script>
    <link rel="stylesheet" href="{!! asset('vendor/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css')!!}">
      <script type="text/javascript">
            $(function () {
                $('#datetime').datetimepicker({
					format: 'DD/MM/YYYY',
					allowInputToggle: true             
				});
            });
        </script>
@stop