<?php

return [
    'site_name' => env('QILARA_SITE_NAME', 'Web Pusat Penelitian Informatika'),
    'institution_short' => env('QILARA_SHORT_NAME', 'SIML'),
    'institution' => 'Pusat Penelitian Informatika'
];