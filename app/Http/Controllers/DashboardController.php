<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\User\Models\Permission;
use App\Modules\User\Models\PermissionRole;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard', [
                "title" => trans('common.home'),        
        ]);
    }    

    public function login()
    {
        return view('login.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required', 'password' => 'required',
        ]);

        //$credentials = $request->only('nip', 'password');
        $remember = false;

        if (\Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')], $remember))
        {
            return redirect()->intended('dashboard');
        }

        return redirect('dashboard/login')
            ->withInput($request->only('username', 'remember'))
            ->withErrors([
                'username' => trans('common.login_failed'),
            ]);
    }

    public function logout()
    {
        \Auth::logout();
        return redirect('dashboard/login');
    }
}
