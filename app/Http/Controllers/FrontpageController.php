<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Instrument\Models\Instrument;
use App\Modules\Instrument\Models\CalibrationHistory;
use App\Modules\Instrument\Models\InstrumentAsset;
use App\Modules\Reservation\Models\Reservation;
use App\Models\Customer;
use Croppa;
use Image;
use Storage;
use Validator;
use Hash;
use Redirect;
use Auth;

class FrontpageController extends Controller
{
    //

    public function __construct()
    {
        \Theme::set('frontend');
    }


    public function index()
    {
        return view('frontend', [
                "title" => ''
        ]);
    }

}