<?php
    
    //Menu Generator
    function parent_link($url, $exact = false) {
        $class = '';
        if ($exact) {
            if (Request::is($url)) {
                $class = ' class="active_menu"';
            }
        } else if (strpos(Request::url(), $url) != false) {
            $class = ' class="active"';
        }

        return $class;
    }

    function menu($link, $icon, $route, $menu_name, $permission = null)
    {
        if (Entrust::can($permission)) {
            return "<li ".parent_link($link)."> 
                    <a href='".route($route)."'><i class='fa ".$icon."'></i>".$menu_name."</a>
                </li>";    
        }
        
    }
    // End Generator

    function year_array()
    {
        $years = DB::table('attendances')
                    ->select([DB::raw('DISTINCT(year(tanggal)) as attendance_year')])
                    ->whereRaw('year(tanggal) > 0')
                    ->orderBy('attendance_year')
                    ->get();
        $year_array = array();
        foreach ($years as $year)
        {
            $year_array[$year->attendance_year] = $year->attendance_year;
        }

        $current_year = date("Y");
        $year_array[$current_year] = $current_year;

        return $year_array;
    }

    function cleanNumber($number)
    {
        if (false !== strpos($number, '.'))
        {
            return rtrim(rtrim($number, '0'), '.');
        }
        else
        {
            return $number;
        }

    }

    function naturalTime($hour)
    {
        if (empty($hour))
        {
            return "";
        }

        $hour = explode(":", $hour);
        $time = intval($hour[0])." ".trans('common.hour');

        if ($hour[1] != "00")
        {
            $time .= " ".intval($hour[1])." ".trans('common.minute');
        }

        return $time;
    }

    function numberToRoman($num)
    {
        $n = intval($num);
        $result = '';

        // Declare a lookup array that we will use to traverse the number:
        $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
            'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
            'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

        foreach ($lookup as $roman => $value)
        {
            // Determine the number of matches
            $matches = intval($n / $value);

            // Store that many characters
            $result .= str_repeat($roman, $matches);

            // Substract that from the number
            $n = $n % $value;
        }

        // The Roman numeral should be built, return it
        return $result;
    }

    function localeDate($date, $display_time = true) {
        if ( empty($date) )  {
            return "";
        } else {
            $check = explode(" ",$date);

            $time = '';

            if (count($check) == '2') {
                $date = explode("-",$check[0]);
                $time = $check[1];
            } else {
                $date = explode("-",$date);
            }

            $month = trans('common.month_array');
            $month_idx = intval($date[1]);

            if($month_idx != 0) {
                $show_date = $date[2]." ".$month[$month_idx]." ".$date[0];

                if ((count($check) == '2') && ($display_time)) {
                    return $show_date. "  ".$time;
                } else {
                    return $show_date;
                }
            }
        }
    }

    function getDay($date)
    {
        return substr($date, 8, 2);
    }

    function getMonth($date)
    {
        /*month_array_short*/
        $month = intval(substr($date, 5, 2));
        return trans('common.month_array_short')[$month];
    }


    function convertToDatepicker($date, $separator = '-', $return_separator = '/')
    {
        if (! empty($date)) {
            $date = explode($separator,$date);

            return $date[2].$return_separator.$date[1].$return_separator.$date[0];
        }
    }

    function terbilang($x)
    {
        $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        if ($x < 12)
            return " " . $abil[$x];
        elseif ($x < 20)
            return terbilang($x - 10) . " belas";
        elseif ($x < 100)
            return terbilang($x / 10) . " puluh" . terbilang($x % 10);
        elseif ($x < 200)
            return " seratus" . terbilang($x - 100);
        elseif ($x < 1000)
            return terbilang($x / 100) . " ratus" . terbilang($x % 100);
        elseif ($x < 2000)
            return " seribu" . terbilang($x - 1000);
        elseif ($x < 1000000)
            return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
        elseif ($x < 1000000000)
            return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
        else
            return "Angka terlalu besar";
    }

    function link_new_window($url)
    {
        $html = '<script type="text/javascript"> w = window.open( "'.$url.'" );w.print();w.close()</script>';

        echo $html;
    }

    function datepickerToMySQL($date)
    {
        $date = explode("/", $date);

        return sprintf("%04d-%02d-%02d", $date[2], $date[1], $date[0]);

    }

    function removeSecond($time) {
        return substr($time, 0, 5);
    }

    //Return array with two value, become array with key and value
    function key_value_array($array)
    {
        $data = array();
        $key = array_keys($array[0]);

        foreach ($array as $val)
        {
            $data[$val[$key[0]]] =  $val[$key[1]];
        }

        return $data;
    }

    function quote($str) {
        return sprintf("'%s'", $str);
    }

    function day_of_week_mysql_to_php($i)
    {
        if ($i == 0) {
            return 7;
        }
        else {
            return $i;
        }
    }

    function defMonth()
    {
        $def_month = date("n");

        if (Request::has('month')) {
            $def_month = Request::input('month');
        }

        return $def_month;
    }

    function defYear()
    {
        $def_year = date("Y");

        if (Request::has('year')) {
            $def_year = Request::input('year');
        }

        return $def_year;
    }

    function parsing_nip($nip)
    {
        if (strlen($nip) == 18) {
            return substr($nip, 0, 8)." ".substr($nip, 8, 6)." ".substr($nip, 14, 1)." ".substr($nip, 15);
        } else {
            return $nip;
        }

    }

    function day_diff($date1, $date2) {
        $date1 = new DateTime($date1);
        $date2 = new DateTime($date2);

        return $date2->diff($date1)->format("%a");
    }

    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    function pretty_array_debug($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        echo "<br />";
    }

    function get_all_middleware()
    {
        $routes = \Route::getRoutes();

        $permission_list = [];

        foreach ($routes as $route) {
            foreach ($route->middleware() as $permission) {                
                if (substr_count($permission, "permission:") > 0) {
                    $permission = str_replace("permission:", "", $permission);

                    if (substr_count($permission, "|") > 0) {
                        $perm_arr = explode("|", $permission);

                        foreach ($perm_arr as $value) {
                            $permission_list[] = $value;
                        }
                    } else {
                        $permission_list[] = $permission;
                    }                    
                }
            }
        }

        return asort($permission_list);        
    }

    function is_checked($id, $collection) {
        if (in_array($id, $collection)) {            
            return "checked";
        }
        return "";
    }

    /**
     * function buildTree
     * @param array $elements
     * @param array $options['parent_id_column_name', 'children_key_name', 'id_column_name'] 
     * @param int $parentId
     * @return array
     */
    function buildTree(array $elements, $options = [
        'parent_id_column_name' => 'parent_id',
        'children_key_name' => 'children',
        'id_column_name' => 'id'], $parentId = 0)
        {
        $branch = array();
        foreach ($elements as $element) {
            if ($element[$options['parent_id_column_name']] == $parentId) {
                $children = buildTree($elements, $options, $element[$options['id_column_name']]);
                if ($children) {
                    $element[$options['children_key_name']] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }

    function printTree($tree,  $selected = null, $r = 0, $p = null) 
    {
        $result = array();

        foreach ($tree as $i => $t) {
            $dash = (empty($t['parent_id'])) ? '' : str_repeat('--', $r) .' ';

            $is_selected = '';
        
            if ($selected == $t['id']) {
                $is_selected = 'selected = selected';
            }

            $result[] = "<option value='".$t['id']."' ". $is_selected .">".$dash.$t['name']."</option>\n";
            if (isset($t['children'])) {
                $result = array_merge($result, printTree($t['children'], $selected, ($r + 1), $t['parent_id']));
            }
        }        

        return $result;                
    }

    function dropdown_tree($name, $tree, $attributes, $selected = null) 
    {
        $html = '<select name='.$name.' '.\Html::attributes($attributes).'>';
        
        $selected_null = '';
        
        if (empty($selected)) {
            $selected_null = 'selected = selected';
        }

        $html .= '<option value=0 '.$selected_null.'></option>';
        $html .= implode('\n', printTree($tree, $selected));
        $html .= '</select>';

        return $html;
    }   

    function tableTree($tree,  $r = 0, $p = null) 
    {
        $result = array();

        foreach ($tree as $i => $t) {
            $dash = (empty($t['parent_id'])) ? '' : str_repeat('-----', $r) .' ';

            $loc = array();

            $loc = $t;
            $loc['name'] = $dash.$t['name'];

            $result[] = $loc;            

            if (isset($t['children'])) {
                $result = array_merge($result, tableTree($t['children'], ($r + 1), $t['parent_id']));
            } 
        }        

        return $result;                
    }

    function get_all_child($table, $id)
    {
        $sql =  "SELECT GROUP_CONCAT(lv SEPARATOR ',') as childs FROM ".
                "(SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ',') FROM ".$table.
                " WHERE FIND_IN_SET(parent_id, @pv)) AS lv FROM ".$table.
                " JOIN (SELECT @pv:= ".$id.")tmp WHERE parent_id IN (@pv)) a";        

        $query = \DB::select($sql);

        return explode(',', $query[0]->childs);
    }