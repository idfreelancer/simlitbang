<?php 

namespace App\Modules\User\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ['name', 'display_name', 'description'];

     public function users()
    {
        return $this->belongsToMany('App\Modules\User\Models\User')->using('App\Modules\User\Models\UserRole');
    }
}