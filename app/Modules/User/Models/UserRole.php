<?php 
namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {
    protected $table = 'role_user';
    
    public $timestamps = false;

    protected $fillable = ['user_id', 'role_id'];

    function users()
    {
        return $this->belongsTo('App\Modules\User\Models\User');
    }

    function roles()
    {
        return $this->belongsTo('App\Modules\User\Models\Role', 'role_id', 'id');
    }
}
