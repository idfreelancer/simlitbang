<?php 
namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model {
    protected $table = 'permission_role';
    
    public $timestamps = false;
    
    protected $fillable = ['permission_id', 'role_id'];


    function permissions()
    {
        return $this->belongsTo('App\Modules\User\Models\Permission', 'permission_id', 'id');
    }

    function roles()
    {
        return $this->belongsTo('App\Modules\User\Models\Role', 'role_id', 'id');
    }
}
