<?php
namespace App\Modules\User\Database\Seeds;
use Illuminate\Database\Seeder;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Role;
use App\Modules\User\Models\UserRole;
use App\Modules\User\Models\Permission;
use App\Modules\User\Models\PermissionRole;
use DB;
use Hash;

class UserDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('roles')->delete();
        DB::table('role_user')->delete();
        DB::table('permissions')->delete();

        User::create(['id' => 1, 'name' => 'Administrator', 'username' => 'admin', 'email' => 'admin@me.com', 'password' => Hash::make('idman')]);
        Role::create(['id' => 1, 'name' => 'admin', 'display_name' => 'Admin', 'description' => 'Manage Application']);
        UserRole::create(['user_id' => 1, 'role_id' => 1]);

        Permission::create(['id' => 1, 'name' => 'logs']);

        Permission::create(['id' => 2, 'name' => 'permissions-create', 'display_name' => 'Create Permissions', 'description' => 'Create New Permission']);
        Permission::create(['id' => 3, 'name' => 'permissions-destroy']);
        Permission::create(['id' => 4, 'name' => 'permissions-edit']);
        Permission::create(['id' => 5, 'name' => 'permissions-index']);
        Permission::create(['id' => 6, 'name' => 'permissions-store']);
        Permission::create(['id' => 7, 'name' => 'permissions-update']);

        Permission::create(['id' => 8, 'name' => 'roles-create']);
        Permission::create(['id' => 9, 'name' => 'roles-destroy']);
        Permission::create(['id' => 10, 'name' => 'roles-edit']);
        Permission::create(['id' => 11, 'name' => 'roles-index']);
        Permission::create(['id' => 12, 'name' => 'roles-store']);
        Permission::create(['id' => 13, 'name' => 'roles-update']);

        Permission::create(['id' => 14, 'name' => 'users-create']);
        Permission::create(['id' => 15, 'name' => 'users-destroy']);
        Permission::create(['id' => 16, 'name' => 'users-edit']);
        Permission::create(['id' => 17, 'name' => 'users-index']);
        Permission::create(['id' => 18, 'name' => 'users-store']);
        Permission::create(['id' => 19, 'name' => 'users-update']);

        Permission::create(['id' => 20, 'name' => 'settings']);

        for ($i = 1; $i <= 20; $i++) {
            PermissionRole::create(['permission_id' => $i, 'role_id' => 1]);
        }
        

    }
}