@extends('../index')

@section('content')
    @if (Session::has('message'))
        <div class="row">
            <div class="alert alert-success" role="alert">
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="row">
            <div class="alert alert-error" role="alert">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ $title }}
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-lg-12 col-md-12">
                        @if (Route::getCurrentRoute()->getName() == 'permissions.edit')
                            {!! Form::model($data, array('url' => route('permissions.update', Request::segment(4)), 'class' => 'form-horizontal', 'method' => 'put')) !!}
                        @else
                            {!! Form::open(array('url' => route('permissions.store'), 'class' => 'form-horizontal', 'method' => 'post')) !!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.name') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('display_name',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.description') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::textarea('description',null, array('id' => 'inputName', 'class' => 'form-control description', 'required' => true)) !!}
                                </div>
                            </div>                            
                            <div class="form-actions pal">
                                <div class="form-group mbn">
                                    <div class="col-md-2 col-xs-2 right">
                                        <input type="hidden" name="items">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>  
@stop