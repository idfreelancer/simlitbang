<?php

namespace App\Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\User\Models\Permission;
use App\Modules\User\Models\PermissionRole;
use App\Modules\User\Models\Role;
use Redirect;
use Validator;

class RoleController extends Controller
{
    

    public function role_index()
    {
        return view('user::roles.index', [
            "title" => trans('user::modules.role'),
            "data" => Role::all()
        ]);
    }

    public function all_permission()
    {
        $data  = Permission::all();
        
        $permission = [];
        $prefix = '';
        $i = 0;

        foreach ($data as $value) {            
            if (substr_count($value->name, '-') > 0) {
                //Contain '-', assume it is a grouped permission
                $start = strpos($value->name, '-');
                $prefix = substr($value->name, 0, $start);

                $permission[$prefix][$i]['id'] = $value->id;
                $permission[$prefix][$i]['name'] = $value->name;
                $permission[$prefix][$i]['display_name'] = $value->display_name;
                $permission[$prefix][$i]['description'] = $value->description;
            } else {
                $permission[$i][0]['id'] = $value->id;
                $permission[$i][0]['name'] = $value->name;
                $permission[$i][0]['display_name'] = $value->display_name;
                $permission[$i][0]['description'] = $value->description;                
            }

            $i++;
        }

        return $permission;
    }

    public function get_permission($role_id)
    {

        $permission = PermissionRole::select('permission_id')->where('role_id','=', $role_id)->get();
        
        $data = [];
        foreach ($permission as $key => $value) {
            $data[] = $value->permission_id;
        }

        return $data;;
    }

    public function role_create()
    {
        return view('user::roles.create', [
            "title" => trans('common.add')." ".trans('user::modules.permission'),            
            'permission'  => $this->all_permission(),
            'cur_permission' => []
        ]);
    }

    public function role_store(Request $request)
    {
        $rules = array(
            'display_name' => 'required|alpha_spaces',
            'description' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $role = new Role();
            $role->name = str_slug($request->input('display_name'),'-');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $role->save();

            foreach ($request->input('permission') as $value) {
                PermissionRole::create(['permission_id' => $value, 'role_id' => $role->id]);
            }       

            return Redirect::route('roles.index')
                ->with('message', trans('user::modules.role').' '.trans('common.is_saved'));
        }
    }

    public function role_edit($id)
    {
        $role = Role::findOrFail($id);

        return view('user::roles.create', [
            'title' => trans('common.edit')." ".trans('user::modules.role'),
            'data' => $role,
            'permission'  => $this->all_permission(),
            'cur_permission' => $this->get_permission($id)
        ]);
    }

    public function role_update(Request $request, $id)
    {
        $rules = array(
            'display_name' => 'required|alpha_spaces',
            'description' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $role = Role::findorFail($id);
            $role->name = str_slug($request->input('display_name'),'-');
            $role->display_name = $request->input('display_name');
            $role->description = $request->input('description');
            $role->save();

            PermissionRole::where('role_id', $id)->delete();

            foreach ($request->input('permission') as $value) {
                PermissionRole::create(['permission_id' => $value, 'role_id' => $role->id]);
            }

            return Redirect::route('roles.index')
                ->with('message', trans('user::modules.role').' '.trans('common.is_updated'));        
        }
    }

    public function role_destroy($id)
    {
        $role = Permission::find($id);
        $role->delete();

        return Redirect::back()->with('message', trans('user::modules.permission').' '.trans('common.is_deleted'));        
    }

    public function permission_index()
    {
        $data = Permission::all();

        return view('user::permissions.index', [
            "title" => trans('user::modules.permission'),
            "data" => $data
        ]);
    }

    public function permission_create()
    {
        return view('user::permissions.create', [
            "title" => trans('common.add')." ".trans('user::modules.permission')
        ]);
    }

    public function permission_store(Request $request)
    {
        $rules = array(
            'display_name' => 'required',
            'description' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $permission = new Permission();
            $permission->name = str_slug($request->input('display_name'),'-');
            $permission->display_name = $request->input('display_name');
            $permission->description = $request->input('description');
            $permission->save();           

            return Redirect::route('permissions.index')
                ->with('message', trans('user::modules.permission').' '.trans('common.is_saved'));
        }
    }

    public function permission_edit($id)
    {
        $permission = Permission::findOrFail($id);

        return view('user::permissions.create', [
            'title' => trans('common.edit')." ".trans('user::modules.permission'),
            'data' => $permission            
        ]);
    }

    public function permission_update(Request $request, $id)
    {
        $rules = array(
            'display_name' => 'required',
            'description' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $permission = Permission::findorFail($id);
            $permission->name = str_slug($request->input('display_name'),'-');
            $permission->display_name = $request->input('display_name');
            $permission->description = $request->input('description');
            $permission->save();           

            return Redirect::route('permissions.index')
                ->with('message', trans('user::modules.permission').' '.trans('common.is_updated'));        
        }
    }

    public function permission_destroy($id)
    {
        $permission = Permission::find($id);
        $permission->delete();

        return Redirect::back()->with('message', trans('user::modules.permission').' '.trans('common.is_deleted'));        
    }

}