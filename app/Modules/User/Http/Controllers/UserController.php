<?php

namespace App\Modules\User\Http\Controllers;

/*
use Illuminate\Http\Request;

use App\Http\Requests;
*/
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Role;
use App\Modules\User\Models\UserRole;
use Validator;
use Hash;
use Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::with('roles')->get();

        return view('user::users.index', [
            "title" => trans('user::modules.users'),
            "data" => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user::users.create', [
            "title" => trans('common.add_user'),
            'role_list' => $this->roleList()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required|min:4',
            'email' => 'email',
            'username' => 'unique:users',         
            'password' => 'required|min:5|confirmed'
        );
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $user = new User();
            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->email = $request->input('email');            
            $user->password = Hash::make($request->input('password'));
            $user->save();
            

            User::create($request->all());

            UserRole::create(['user_id' => $user->id, 'role_id' => $request->input('role')]);

            return Redirect::route('users.index')
                ->with('message', trans('common.user_created'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('roles')->findOrFail($id);

        return view('user::users.create', [
            "title" => trans('common.edit_user'),
            'role_list' => $this->roleList(),
            'data' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required|min:4',
            'email' => 'email',
            'username' => 'unique:users,username,'.$id,            
            'password' => 'confirmed'
        );

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $user = User::findorFail($id);
            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            
            if ($request->has('password'))
                $user->password = Hash::make($request->input('password'));

            $user->save();

            //$user_role = User::findorFail($id);
            $role = UserRole::where('user_id',$id)->first();
            if (! $role) {                
                UserRole::create(['user_id' => $id, 'role_id' => $request->input('role')]);
            } else {
                UserRole::where('user_id',$id)->update(['role_id' => $request->input('role')]);
            }
            

            return Redirect::route('users.index')
                ->with('message', trans('common.user_updated'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return \Redirect::back()->with('message', trans('user::modules.users'). ' '.trans('common.is_deleted'));
    }

    public  function storePicture()
    {
        $destinationPath = 'uploads';
        $fileName = \Auth::user()->id."-".Input::file('avatar')->getClientOriginalName();

        $validator = Validator::make(Input::all(),
            array('avatar' => array('required', 'max:4096','image'))
        );

        if ($validator->fails()) {
            header('HTTP/1.1 400 Bad Request');
            exit("Dibutuhkan file image");
        } else {
            if (Input::file('avatar')->move($destinationPath, $fileName))
            {
                $user = User::find(\Auth::user()->id);
                $user->foto = $fileName;
                $user->save();

                $image = new ImageResize($destinationPath. "/" .$fileName);
                $image->resizeToWidth(200);
                $image->save($destinationPath. "/" .$fileName);

                echo $fileName;
            }
        }
    }

    public function roleList()
    {
        $data = array();

        foreach (Role::all() as $val)
        {
            $data[$val->id] = $val->display_name;
        }

        return $data;
    }
}
