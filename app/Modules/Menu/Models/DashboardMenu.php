<?php

namespace App\Modules\Menu\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardMenu extends Model
{
    protected $fillable = [
        'name', 'route', 'icon', 'permission', 'parent_id', 'order'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Modules\Menu\Models\DashboardMenu', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Modules\Menu\Models\DashboardMenu', 'parent_id');
    }
}
