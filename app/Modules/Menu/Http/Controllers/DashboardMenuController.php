<?php

namespace App\Modules\Menu\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Menu\Models\DashboardMenu;
use Validator;
use Hash;
use Redirect;
use DB;

class DashboardMenuController extends Controller
{
    public $rules = array(
            'name' => 'required|min:2'
        );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DashboardMenu::orderBy('order')->get()->toArray();
        $tree = buildTree($data);

        return view('menu::index', [
            "title" => trans('menu::modules.dashboard_menu'),
            "tree" => $tree
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $max_order = DashboardMenu::where('parent_id','=', 0)
                                        ->max('order');

            $menu = new DashboardMenu($request->all());
            
            //Parent Exist
            if ($request->input('parent') <> 0) {
                $menu->parent_id = $request->input('parent');
                $max_order = DashboardMenu::where('parent_id','=',$request->input('parent'))
                                            ->max('order');
                
            }

            $menu->order = $max_order + 1;

            $menu->save();            

            return \Redirect::route('dashboard_menu.index')
                ->with('message',  trans('menu::modules.dashboard_menu'). ' '.trans('common.is_created'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dash_menu_array = DashboardMenu::orderBy('order')->get()->toArray();
        $tree = buildTree($dash_menu_array);

        $data = DashboardMenu::findOrFail($id);

        return view('menu::index', [
            "title" => trans('common.edit'). ' '. trans('menu::modules.dashboard_menu'),
            'data' => $data,
            'tree' => $tree
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $this->rules;
        $rules['parent'] = 'not_my_self:'.$id.'|not_my_child:dashboard_menus,'.$id;

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return \Redirect::back()->withInput()->withErrors($validator);
        }
        else
        {
            $menu = DashboardMenu::findorFail($id);  
            
            #Parent Change, Change the Order
            if ($request->input('parent') <> $menu->parent_id) {
                if ($request->input('parent') <> 0) {
                    $menu->parent_id = $request->input('parent');
                    $max_order = DashboardMenu::where('parent_id','=',$request->input('parent'))
                                                ->max('order');
                
                } else {
                    $menu->parent_id = 0;
                    $max_order = DashboardMenu::where('parent_id','=', 0)
                                        ->max('order');
    
                }

                $menu->order = $max_order + 1;
            }

            $menu->fill($request->all());

            $menu->save();            

            return \Redirect::route('dashboard_menu.index')
                ->with('message',  trans('menu::modules.dashboard_menu'). ' '.trans('common.is_updated'));
        
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DashboardMenu::find($id)->delete();

        return \Redirect::back()->with('message', trans('menu::modules.dashboard_menu'). ' '.trans('common.is_deleted'));
    }

    public function ajaxDelete(Request $request)
    {
        $id = intval($request->input('id'));
        
        $data = DashboardMenu::select('id')->with('children')->find($id)->toArray();

        $idx = array_merge([$id], array_column($data['children'], 'id'));

        if (DashboardMenu::destroy($idx))  {
            echo "1";
        } else {
            echo "0";
        }
    }

    public function ajaxReorder(Request $request)
    {
        $data = json_decode($request->input('data'));

        foreach ($data as $key => $val)
        {
            $menu = DashboardMenu::findOrFail($val->id);
            
            $parent_id = $val->parent_id; 
            
            if (is_null($parent_id)) {
                $parent_id = 0; 
            }

            $menu->parent_id = $parent_id;
            $menu->order = $key + 1;
            $menu->save();
        }
        
    }
}
