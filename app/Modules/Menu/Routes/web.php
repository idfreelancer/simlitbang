<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function() {
	Route::group(['prefix' => 'dashboard_menu'], function () {
		Route::get('/', ['uses' => 'DashboardMenuController@index', 'as' => 'dashboard_menu.index', 'middleware' => ['permission:dashboard_menu-index']]);
		Route::post('store', ['uses' => 'DashboardMenuController@store', 'as' => 'dashboard_menu.store', 'middleware' => ['permission:dashboard_menu-store']]);
		Route::get('{id}/edit', ['uses' => 'DashboardMenuController@edit', 'as' => 'dashboard_menu.edit', 'middleware' => ['permission:dashboard_menu-edit']]);
		Route::match(['put', 'patch'],'{id}', ['uses' => 'DashboardMenuController@update', 'as' => 'dashboard_menu.update', 'middleware' => ['permission:dashboard_menu-update']]);
		Route::delete('{id}', ['uses' => 'DashboardMenuController@destroy', 'as' => 'dashboard_menu.destroy', 'middleware' => ['permission:dashboard_menu-destroy']]);
		Route::post('ajax_delete', ['uses' => 'DashboardMenuController@ajaxDelete', 'as' => 'dashboard_menu.ajax_delete', 'middleware' => ['permission:dashboard_menu-destroy']]);
		Route::post('ajax_reorder', ['uses' => 'DashboardMenuController@ajaxReorder', 'as' => 'dashboard_menu.ajax_reorder', 'middleware' => ['permission:dashboard_menu-create']]);
	});
});