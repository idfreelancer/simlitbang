@extends('../index')

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3>
                @if (Route::getCurrentRoute()->getName() == 'dashboard_menu.edit')
                    {{ trans('common.edit') }}
                    {{ trans('menu::modules.dashboard_menu') }} : 
                    <span class="label label-info white">{{ $data['name']}}</span>
                @else
                    {{ trans('common.add') }}
                    {{ trans('menu::modules.dashboard_menu') }}
                @endif
            </h3>
        </div>
    </div>
    
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="col-lg-12 col-md-12">
                        @if (Route::getCurrentRoute()->getName() == 'dashboard_menu.edit')
                            {!! Form::model($data, array('url' => route('dashboard_menu.update', Request::segment(3)), 'class' => 'form-horizontal', 'method' => 'put')) !!}
                        @else
                            {!! Form::open(array('url' => route('dashboard_menu.store'), 'class' => 'form-horizontal', 'method' => 'post')) !!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.name') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('name',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.route') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('route',null, array('id' => 'inputName', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.permission') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('permission',null, array('id' => 'inputName', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.icon') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('icon',null, array('id' => 'inputName', 'class' => 'form-control', 'placeholder' => 'Only Applicable to Parent')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.parent') }}
                                </label>
                                <div class="col-md-10 col-xs-10">                                    
                                    {!! dropdown_tree('parent', $tree, ['class' => 'form-control'], isset($data->parent_id)?$data->parent_id:null) !!}                                    
                                </div>
                            </div>                            
                            <div class="form-actions pal">
                                <div class="form-group mbn">
                                    <div class="col-md-2 col-xs-2 right">
                                        <input type="hidden" name="items">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>  
@stop

@section('footer_asset')
    {!! Theme::css('css/bootstrap-datepicker.min.css')!!}
    {!! Theme::css('css/jquery.bootstrap-touchspin.css')!!}
    {!! Theme::js('js/bootstrap-datepicker.min.js')!!}
    {!! Theme::js('js/jquery.bootstrap-touchspin.min.js')!!}
    {!! Theme::js('js/qilara.js')!!}
@stop