@extends('../index')

@section('content')
<div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>{{ $title }}</h3>
      </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    {{ trans('common.add') }}  {{ trans('menu::modules.dashboard_menu') }}
                </div>
                <div class="x_content">
                    <div class="col-lg-12 col-md-12">
                        @if (Route::getCurrentRoute()->getName() == 'dashboard_menu.edit')
                            {!! Form::model($data, array('url' => route('dashboard_menu.update', Request::segment(3)), 'class' => 'form-horizontal', 'method' => 'put')) !!}
                        @else
                            {!! Form::open(array('url' => route('dashboard_menu.store'), 'class' => 'form-horizontal', 'method' => 'post')) !!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.name') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('name',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.route') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('route',null, array('id' => 'inputName', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.permission') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('permission',null, array('id' => 'inputName', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.icon') }}
                                </label>
                                <div class="col-md-10 col-xs-10">
                                    {!! Form::text('icon',null, array('id' => 'inputName', 'class' => 'form-control', 'placeholder' => 'Only Applicable to Parent')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-2 control-label col-xs-2">
                                    {{ trans('common.parent') }}
                                </label>
                                <div class="col-md-10 col-xs-10">                                    
                                    {!! dropdown_tree('parent', $tree, ['class' => 'form-control'], isset($data->parent_id)?$data->parent_id:null) !!}                                    
                                </div>
                            </div>                            
                            <div class="form-actions pal">
                                <div class="form-group mbn">
                                    <div class="col-md-2 col-xs-2 right">
                                        <input type="hidden" name="items">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-xs-6">
            <div class="x_panel">
                <div class="x_title">
                    {{ trans('menu::modules.structure') }}  {{ trans('menu::modules.dashboard_menu') }}
                </div>
                <div class="x_content">
                    <div class="col-md-12">
                        <ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">
                            @foreach ($tree as $parent_menu)
                                <li class="mjs-nestedSortable-leaf" id="menuItem_{{ $parent_menu['id'] }}">
                                    <div class="alert alert-info">
                                        <span class="disclose">
                                            <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                                        </span>
                                        <span>
                                            <span class="itemTitle">
                                                <i class="{{ $parent_menu['icon'] }}"></i>
                                                <a href="{{route('dashboard_menu.edit', $parent_menu['id'])}}" class="label label-success">
                                                    {{ $parent_menu['name'] }}
                                                </a>
                                                <span class="label label-danger">{{ $parent_menu['route'] }}</span>
                                                <span class="label label-primary">{{ $parent_menu['permission'] }}</span>
                                            </span>
                                            <span class="deleteMenu close" data-id="{{ $parent_menu['id'] }}" aria-hidden="true">×</span>
                                        </span>
                                    </div>

                                <!-- Has Children -->
                                @if (isset($parent_menu['children']))
                                    <ol class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">
                                        @foreach ($parent_menu['children'] as $child_menu)
                                            <li class="mjs-nestedSortable-leaf" id="menuItem_{{ $child_menu['id'] }}">
                                                <div class="alert alert-info">
                                                    <span class="disclose">
                                                        <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                                                    </span>
                                                    <span>
                                                        <span class="itemTitle">
                                                            <a href="{{route('dashboard_menu.edit', $child_menu['id'])}}" class="label label-success">
                                                                {{ $child_menu['name'] }}
                                                            </a>
                                                            <span class="label label-danger">{{ $child_menu['route'] }}</span>
                                                            <span class="label label-primary">{{ $child_menu['permission'] }}</span>
                                                        </span>
                                                        <span class="deleteMenu close" data-id="{{ $child_menu['id'] }}" aria-hidden="true">×</span>
                                                    </span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ol>
                                    </li>
                                <!-- Dont Have Children -->
                                @else
                                    </li>
                                @endif
                            @endforeach
                        </ol>
                        <p class="small">*{{ trans('common.refresh_to_changes') }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_asset')
    <link  media="all" type="text/css" rel="stylesheet"  href="{!! asset('vendors/modules/menu/css/menu.css')!!}">
    <script type="text/javascript" src="{!! asset('vendors/sweetalert2/dist/sweetalert2.all.min.js')!!}"></script>
    <script type="text/javascript" src="{!! asset('vendors/modules/menu/js/jquery-ui-1.10.4.custom.min.js')!!}"></script>
    <script type="text/javascript" src="{!! asset('vendors/jquery-nested-sortable/jquery.mjs.nestedSortable.js')!!}"></script>

    <script type="text/javascript">
        $().ready(function(){
            var ns = $('ol.sortable').nestedSortable({
                forcePlaceholderSize: true,
                handle: 'div',
                helper: 'clone',
                items: 'li',
                opacity: .6,
                placeholder: 'placeholder',
                revert: 250,
                tabSize: 25,
                tolerance: 'pointer',
                toleranceElement: '> div',
                maxLevels: 4,
                isTree: true,
                expandOnHover: 700,
                startCollapsed: false,
                excludeRoot: true,
                relocate: function(){
                    var data = $('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});

                    $.post("{{ route('dashboard_menu.ajax_reorder') }}", { data : JSON.stringify(data), 
                                                                        _token : "{{ csrf_token() }}" });
                }
            });
            

            $('.disclose').on('click', function() {
                $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
                $(this).children("span").toggleClass('glyphicon-triangle-bottom').toggleClass('glyphicon-triangle-right');
            });
            
            $('.deleteMenu').click(function(){
                var id = $(this).attr('data-id');

                swal({
                      title: 'Are you sure?',
                      text: "You won't be able to revert this!",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Yes, delete it!'
                    }).then(function () {
                        $.post("{{ route('dashboard_menu.ajax_delete') }}", { id : id, _token : "{{ csrf_token() }}" })
                            .done(function(data){
                                if (data == "1") {
                                    $('#menuItem_'+id).remove();

                                      swal(
                                        'Deleted!',
                                        'Your menu has been deleted.',
                                        'success'
                                      )
                                }
                        });
                    }).catch(swal.noop)
            });
        });     
    </script>
@stop