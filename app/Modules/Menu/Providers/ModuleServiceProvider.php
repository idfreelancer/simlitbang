<?php

namespace App\Modules\Menu\Providers;

use Caffeinated\Modules\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Spatie\Menu\Laravel\Menu;
use App\Modules\Menu\Models\DashboardMenu;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'menu');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'menu');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'menu');

        $this->publishes([
            __DIR__.'/../Assets' => public_path('vendors/modules/menu'),
        ], 'modules');

        
        Menu::macro('dashboard', function() {
            $dash_menu = Menu::new()
                ->wrap('div', ['class' => 'menu_section'])
                ->addClass('nav')
                ->addClass('side-menu');
            $dash_menu->route('dashboard', '<i class="fa fa-home"></i> Home');


            $data = DashboardMenu::orderBy('order')->get()->toArray();

            foreach (buildTree($data) as $parent_menu) {
                $menu_name = '<i class="'.$parent_menu['icon'].'"></i> '.$parent_menu['name'];

                //Has Children
                if (isset($parent_menu['children']))
                {
                    $dash_menu->submenu('<a>'.$menu_name.'<span class="fa fa-chevron-down"></span></a>',
                        function (Menu $menu) use ($parent_menu) {
                            
                            $menu->addClass('nav')
                                    ->addClass('child_menu');

                            foreach ($parent_menu['children'] as $child_menu) {
                                if (!empty($child_menu['permission'])) {
                                    $menu->routeIfCan($child_menu['permission'], $child_menu['route'], $child_menu['name']);
                                } else {
                                    $menu->route($child_menu['route'], $child_menu['name']);
                                }
                            }
                        }
                    );
                }   
                //Don't have children
                else 
                {
                    if (!empty($parent_menu['permission'])) {
                        $dash_menu->routeIfCan($parent_menu['permission'], $parent_menu['route'], $menu_name);
                    } else {
                        $dash_menu->route($parent_menu['route'], $menu_name);
                    }
                    
                }
            }

            return $dash_menu;
        });
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
