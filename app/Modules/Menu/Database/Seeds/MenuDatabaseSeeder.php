<?php

namespace App\Modules\Menu\Database\Seeds;

use Illuminate\Database\Seeder;

class MenuDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MenuPermissions::class);
        $this->call(MenuDashboardMenu::class);
    }
}
