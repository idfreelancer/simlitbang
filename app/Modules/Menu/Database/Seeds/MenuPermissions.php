<?php

namespace App\Modules\Menu\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\User\Models\Permission;
use App\Modules\User\Models\PermissionRole;

class MenuPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_arr = ['dashboard_menu-create', 'dashboard_menu-destroy',
                           'dashboard_menu-edit', 'dashboard_menu-index',
                           'dashboard_menu-store', 'dashboard_menu-update'];

        foreach ($permission_arr as $value) {
            $permission = Permission::create(['name' => $value]);

            PermissionRole::create(['permission_id' => $permission->id, 'role_id' => 1]);
        }
    }
}
