<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;


class install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'qilara:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install this laravel starter kit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Hello There. Install will begin shortly");
        $this->info("=======================================");
        $this->info("Generate Key");
        $this->call('key:generate');
        $this->info("\nInstall Module User");
        $this->call('module:optimize');
        $this->call('module:enable', ['slug' => 'user']);
        $this->call('module:migrate', ['slug' => 'user']);
        $this->call('module:seed', ['slug' => 'user']);

        $this->info("\nInstall Module Menu");
        $this->call('module:enable', ['slug' => 'menu']);
        $this->call('module:migrate', ['slug' => 'menu']);
        $this->call('module:seed', ['slug' => 'menu']);
    }
}
