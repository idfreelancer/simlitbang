<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Modules\User\Models\Permission;
use Illuminate\Support\Facades\Schema;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        if (Schema::hasTable('permissions')) {

            $gate->before(function ($user) {
                if ($user->hasRole('owner')) {
                    return true;
                }
            });

            $this->registerPolicies($gate);

            foreach( $this->getPermissions() as $permission)
            {
                $gate->define($permission->name, function ($user) use ($permission) {
                    return $user->can($permission->name) ;
                });
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
