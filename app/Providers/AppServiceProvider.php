<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Validator::extend('alpha_spaces', function ($attribute, $value) {
            // This will only accept alpha and spaces. 
            // If you want to accept hyphens use: /^[\pL\s-]+$/u.
            return preg_match('/^[\pL\s]+$/u', $value);
        });

        Validator::extend('not_my_child', function ($attribute, $value, $parameters, $validator) {
            //Return True if not_my_child
            $childs = get_all_child($parameters[0], $parameters[1]);            

            if (in_array($value, $childs)) {
                // Ouch, I have child    
                return false;
            } else {
                return true;
            }
        });

        Validator::extend('not_my_self', function ($attribute, $value, $parameters, $validator) {
            if ( $value == $parameters[0] ) {                
                return false;
            } else {
                return true;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
